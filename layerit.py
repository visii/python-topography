import math
import numpy as np
import rasterio
import matplotlib.pyplot as plt
from os import path

from typing import NamedTuple
# from skimage import color, morphology
from skimage.io import imread, imsave, imshow
from skimage.util import crop


# see
#   https://www.mapzen.com/blog/tangram-heightmapper/
#   https://scikit-image.org/docs/dev/auto_examples/index.html

TOPO_URL = "https://tangrams.github.io/heightmapper/#7/64.9/-17.7"
TOPO_PNG = "iceland.png"

max_elev = 1966   # 255 aka white
min_elev =    0   #   0 aka black
z_x_ratio = 0.0010167657414246647

min_to_max_elev = max_elev - min_elev
mpg = min_to_max_elev
mpx = mpg / z_x_ratio

print("mpg is " + str(mpg))

sea_level_gray = (0 - min_elev) / mpg

if not path.exists(TOPO_PNG):
    print("need " + TOPO_PNG + " from  " + TOPO_URL)
    exit(1)

topo_img = imread(TOPO_PNG, as_gray=True)

topo_height, topo_width = topo_img.shape
layer_map = np.zeros([topo_height, topo_width, 3], dtype=np.uint8)

n_layers = 5
m_per_layer = 400  #  roughly max_elev / n_layers

for layer in range(n_layers):

    layer_floor_m = layer * m_per_layer
    layer_floor_g = layer_floor_m / mpg
    layer_mask = topo_img > layer_floor_g

    layer_cutout = np.zeros([topo_height, topo_width], dtype=np.uint8)
    layer_cutout[layer_mask] = 255
    imsave("layer" + str(layer) + ".png", layer_cutout)

    # (layer * 25, (layer % 4) * 60, (n_layers - layer) * 20)
    layer_gray = (layer + 1) * 40
    layer_color = (layer_gray, layer_gray, layer_gray)
    print("layer ", layer, " floor ", layer_floor_g, " color ", layer_color)
    layer_map[layer_mask] = layer_color

# all done!

imsave("layers.png", layer_map)



